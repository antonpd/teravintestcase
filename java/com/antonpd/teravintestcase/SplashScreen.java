package com.antonpd.teravintestcase;

/**
 * Created by antonpd on 24/10/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SplashScreen extends Activity {

    Network net = new Network(SplashScreen.this);
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if(net.isDeviceOnline()){
            new HttpAsyncTask().execute();
            new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                @Override
                public void run() {
                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();

                }
            }, SPLASH_TIME_OUT);
        }else{
            Toast.makeText(getApplicationContext(),"Pastikan perangkat anda terhubung dengan internet",Toast.LENGTH_SHORT).show();
            finish();
        }


    }

    private class HttpAsyncTask extends AsyncTask<Void, Void, Void> {

        DatabaseHelper dbs;
        List<Film> f = new ArrayList<>();
        public HttpAsyncTask(){
            this.dbs = new DatabaseHelper(getApplicationContext());
        }

        @Override
        protected Void doInBackground(Void... params) {

            try{
                dbs.dropAllTable();
                f = net.requestMovie();
                for (int i=0;i<f.size();i++){
                    dbs.addFilm(f.get(i));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


    }
}

