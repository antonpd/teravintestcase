package com.antonpd.teravintestcase;

/**
 * Created by antonpd on 24/10/2016.
 */

public class Film {
    int id;
    String judul_film;
    String tanggal_main;

    public Film(){

    }

    public Film(int id,String judul_film,String tanggal_main){
        this.id = id;
        this.judul_film = judul_film;
        this.tanggal_main = tanggal_main;
    }

    public Film(String judul_film,String tanggal_main){
        this.judul_film = judul_film;
        this.tanggal_main = tanggal_main;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul_film() {
        return judul_film;
    }

    public void setJudul_film(String judul_film) {
        this.judul_film = judul_film;
    }

    public String getTanggal_main() {
        return tanggal_main;
    }

    public void setTanggal_main(String tanggal_main) {
        this.tanggal_main = tanggal_main;
    }
}
