package com.antonpd.teravintestcase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonpd on 24/10/2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private Context context;
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "filmManager";

    // Contacts table name
    private static final String TABLE_FILM = "film";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String JUDUL_FILM = "judul_film";
    private static final String TANGGAL_MAIN = "tanggal_main";

    private static final String CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_FILM + "("
            + KEY_ID + " INTEGER," + JUDUL_FILM + " TEXT,"
            + TANGGAL_MAIN + " DATE" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FILM);

        // Create tables again
        onCreate(db);
    }


    public long addFilm(Film film) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.i("isi data film",film.getJudul_film());
        ContentValues values = new ContentValues();
        values.put(JUDUL_FILM, film.getJudul_film());
        values.put(TANGGAL_MAIN, film.getTanggal_main());

        // Inserting Row
        long ROW_FILM = db.insert(TABLE_FILM, null, values);
        close(); // Closing database connection

        return ROW_FILM;
    }


    public Film getFilm(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_FILM, new String[] { KEY_ID,
                        JUDUL_FILM, TANGGAL_MAIN }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Film contact = new Film(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return contact
        return contact;
    }

    public List<Film> getAllFilm() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Film> filmList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_FILM;

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Film film = new Film();
                film.setId((cursor.getInt(cursor.getColumnIndex(KEY_ID))));
                film.setJudul_film(cursor.getString(cursor.getColumnIndex(JUDUL_FILM)));
                film.setTanggal_main(cursor.getString(cursor.getColumnIndex(TANGGAL_MAIN)));
                // Adding contact to list
                filmList.add(film);
            } while (cursor.moveToNext());
            close();
        }

        return filmList;
    }

    public int getFilmsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_FILM;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    public int updateFilm(Film film) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(JUDUL_FILM, film.getJudul_film());
        values.put(TANGGAL_MAIN, film.getTanggal_main());

        // updating row
        return db.update(TABLE_FILM, values, KEY_ID + " = ?",
                new String[] { String.valueOf(film.getId()) });
    }

    public void deleteFilm(Film film) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FILM, KEY_ID + " = ?",
                new String[] { String.valueOf(film.getId()) });
        db.close();
    }

    public void dropAllTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FILM, null, null);

        db.close();
    }
}
