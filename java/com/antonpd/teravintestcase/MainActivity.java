package com.antonpd.teravintestcase;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    Network net = new Network(MainActivity.this);
    private AdapterFilm adapterFilm;
    DatabaseHelper db;
    ListView list;
    List<Film> list_film;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.db = new DatabaseHelper(this);

        list_film = this.db.getAllFilm();

        Log.i("data judul",list_film.get(0).getJudul_film());
        Log.i("data tanggal",list_film.get(0).getTanggal_main());



        this.adapterFilm = new AdapterFilm(this,R.layout.adapter_film,list_film);

        list = (ListView)findViewById(R.id.list_view_item);
        list.setAdapter(adapterFilm);
        adapterFilm.notifyDataSetChanged();

        Timer repeatTask = new Timer();
        int repeatInterval = 60000;
        repeatTask.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                List<Film> film = new ArrayList<>();
                try {
                    db.dropAllTable();
                    film = net.requestMovie();
                    for (int i=0;i<film.size();i++){
                        db.addFilm(film.get(i));
                    }
//                    Intent i = new Intent(getApplicationContext(), NotificationActivity.class);
//                    finish();
//                    startActivity(i);
                    Log.i("Service","update");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        },0,repeatInterval);
    }



}
class AdapterFilm extends ArrayAdapter<Film>{
    private ArrayList<Film> list;
    private Activity activity;

    public AdapterFilm(Context context, int textViewResourceId,List<Film> objects){
        super(context, textViewResourceId, objects);

        this.activity=(Activity) context;
        this.list=(ArrayList<Film>) objects;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        try{
            View v = convertView;
            if(v == null){
                LayoutInflater vi = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.adapter_film, null);
            }

            final Film f = list.get(position);
            if(f!=null){
                TextView tt = (TextView) v.findViewById(R.id.judul_film);
                tt.setText(f.getJudul_film());
                tt = (TextView) v.findViewById(R.id.tanggal_main);
                tt.setText(f.getTanggal_main());
            }
            return v;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}