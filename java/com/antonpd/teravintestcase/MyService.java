package com.antonpd.teravintestcase;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonpd on 25/10/2016.
 */

public class MyService extends IntentService {
    Network net = new Network(MyService.this);
    DatabaseHelper dbs;
    List<Film> f = new ArrayList<>();
    public MyService(){
        super("MyService");
        this.dbs = new DatabaseHelper(getApplicationContext());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Do the task here
        if(net.isDeviceOnline()){
            try{
                dbs.dropAllTable();
                f =  net.requestMovie();
                for (int i=0;i<f.size();i++){
                    dbs.addFilm(f.get(i));
                }
                Log.i("Service","UPDATE");
                Toast.makeText(getApplicationContext(),"UPDATED",Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else{
            Toast.makeText(getApplicationContext(),"Pastikan perangkat anda terhubung dengan internet",Toast.LENGTH_SHORT).show();
        }
        Log.i("MyService", "Service running");
    }
}
