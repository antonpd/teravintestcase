package com.antonpd.teravintestcase;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by antonpd on 25/10/2016.
 */

public class NotificationActivity extends Activity implements View.OnClickListener {
    private Button tampilkan;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);
        tampilkan = (Button)findViewById(R.id.button);
        tampilkan.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view==tampilkan){
            Intent i = new Intent(this, MainActivity.class);
            finish();
            startActivity(i);
        }
    }
}
