package com.antonpd.teravintestcase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by antonpd on 25/10/2016.
 */

public class MyReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "com.antonpd.teravintestcase.alarm";
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, MyService.class);
        i.putExtra("foo", "bar");
        context.startService(i);
    }
}
