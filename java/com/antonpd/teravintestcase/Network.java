package com.antonpd.teravintestcase;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonpd on 24/10/2016.
 */

public class Network {
    private static final String BASE_URL_WEB_SERVICE = "https://api.themoviedb.org/3/movie/popular?api_key=f7b67d9afdb3c971d4419fa4cb667fbf&language=en-US&page=1";

    private Context context;
    private DatabaseHelper db;

    public Network(Context context) {
        this.context = context;
        this.db= new DatabaseHelper(context);
    }

    public boolean isDeviceOnline() {
        boolean result = false;
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            result = true;
        }
        return result;
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }
        inputStream.close();
        return result;
    }

    private String httpRequest(String address, String method,
                               String body) throws IOException, JSONException {
        URL url = new URL(address);
        Log.i("req address", address == null ? ": address" : ": " + address);
        Log.i("req method", method == null ? ": null" : ": " + method);
        Log.i("req body", method == null ? ": body" : ": " + body);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(method);
        conn.setRequestProperty("Content-Type", "application/json");
        if (body != null) {
            byte[] bodyInBytes = body.getBytes();
            OutputStream os = conn.getOutputStream();
            os.write(bodyInBytes);
            os.close();
        }
        Log.i("Respon Header", ": " + conn.getResponseCode());
        int httpResponCode = conn.getResponseCode();
        if (httpResponCode < 200 || httpResponCode > 226) { // If the response code isn't 200 or 2xx, use getErrorStream() instead of getInputStream().
            JSONObject errorJson = new JSONObject(convertInputStreamToString(conn.getErrorStream()));
            return null;
        }
        InputStream in = new BufferedInputStream(conn.getInputStream());
        String respon = convertInputStreamToString(in);
        Log.i("Respon Body", ": " + respon);
        conn.disconnect();
        return respon;
    }

    public List<Film> requestMovie()throws JSONException, IOException{
        String address = BASE_URL_WEB_SERVICE;
        List<Film> Listfilm = new ArrayList<>();
        JSONObject ja = new JSONObject(httpRequest(address, "GET", null));
        for (int i=0;i<10;i++){
            Film film = new Film();
            Log.i("deb",ja.getJSONArray("results").getJSONObject(i).getString("original_title"));
            film.setId(Integer.parseInt(ja.getJSONArray("results").getJSONObject(i).getString("id")));
            film.setJudul_film(ja.getJSONArray("results").getJSONObject(i).getString("original_title"));
            film.setTanggal_main(ja.getJSONArray("results").getJSONObject(i).getString("release_date"));
            Listfilm.add(film);
        }
        Log.i("size ",""+Listfilm.size());
        return Listfilm;
    }
}
